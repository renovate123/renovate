module.exports = {
  onboardingConfig: {
    extends: ["config:base"],
  },
  platform: "gitlab",
  gitAuthor: "RenovateBot <renovatebot@gmail.com>",
  baseBranches: ["main"],
  labels: ["dependencies"],
  autodiscover: true
}
